const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require("./models/Track");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);
  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collections) {
    await mongoose.connection.dropCollection(coll.name);
  }

  const [Beyonce, ChrisBrown, Rihanna] = await Artist.create({
    name: 'Beyonce',
    image: 'beyonce.jpg',
    information: 'American singer-songwriter and actress Beyoncé first became known as the lead singer of the R&B group Destiny\'s Child before she embarked on what would prove to be a hugely successful solo career, beginning with the album "Dangerously in Love," which was released in 2003 to rave reviews.'
  }, {
    name: 'Chris Brown',
    image: 'chrisBrown.jpg',
    information: 'Chris Brown, in full Christopher Maurice Brown, (born May 5, 1989, Tappahannock, Virginia, U.S.), American rhythm-and-blues (R&B) singer, songwriter, and actor whose melodic voice and skilled dancing propelled him to fame, though his success was sometimes overshadowed by his tumultuous personal life.'
  }, {
    name: 'Rihanna',
    image: 'rihanna.jpg',
    information: 'Rihanna, byname of Robyn Rihanna Fenty, (born February 20, 1988, St. Michael parish, Barbados), Barbadian pop and rhythm-and-blues (R&B) singer who became a worldwide star in the early 21st century, known for her distinctive and versatile voice and for her fashionable appearance.'
  });

  const [lemonade, royalty, loud, dangerouslyInLove] =  await Album.create({
    title: 'Lemonade',
    artist: Beyonce,
    yearOfIssue: 2016,
    image: 'lemonade.jpg',
    is_published: true,
  }, {
    title: 'Royalty',
    artist: ChrisBrown,
    yearOfIssue: 2015,
    image: 'royalty.jpg',
    is_published: true,
  }, {
    title: 'Loud',
    artist: Rihanna,
    yearOfIssue: 2010,
    image: 'loud.jpg',
    is_published: false,
  }, {
    title: 'Dangerously in Love',
    artist: Beyonce,
    yearOfIssue: 2003,
    image: 'DangerouslyInLove.jpg',
    is_published: false,
  });

  await Track.create({
    title: 'Hold up',
    album: lemonade,
    duration: '3:41',
    url: 'https://www.youtube.com/embed/PeonBmeFR8o',
    is_published: true,
  }, {
    title: 'Sorry',
    album: lemonade,
    duration: '3:53',
    url: 'https://www.youtube.com/embed/QxsmWxxouIM',
    is_published: true,
  }, {
    title: 'Pray you catch me',
    album: lemonade,
    duration: '3:16',
    url: 'https://www.youtube.com/embed/170vgLFJW2Y',
    is_published: true,
  }, {
    title: 'Daddy lessons',
    album: lemonade,
    duration: '4:16',
    is_published: false,
  }, {
    title: 'Freedom',
    album: lemonade,
    duration: '4:10',
    is_published: false,
  }, {
    title: 'Make love',
    album: royalty,
    duration: '3:50',
    is_published: false,
  }, {
    title: 'Wrist',
    album: royalty,
    duration: '3:14',
    is_published: true,
  }, {
    title: 'Zero',
    album: royalty,
    duration: '3:10',
    is_published: true,
  }, {
    title: 'Day one',
    album: royalty,
    duration: '4:07',
    is_published: true,
  }, {
    title: 'KAE',
    album: royalty,
    duration: '3:34',
    is_published: false,
  }, {
    title: 'S&M',
    album: loud,
    duration: '3:14',
    is_published: false,
  }, {
    title: 'Fading',
    album: loud,
    duration: '3:37',
    is_published: false,
  }, {
    title: 'Man down',
    album: loud,
    duration: '3:50',
    is_published: false,
  }, {
    title: 'Skin',
    album: loud,
    duration: '5:06',
    is_published: false,
  }, {
    title: 'Cheers',
    album: loud,
    duration: '3:56',
    is_published: false,
  }, {
    title: 'Be with you',
    album: dangerouslyInLove,
    duration: '3:45',
    is_published: false,
  }, {
    title: 'Baby boy',
    album: dangerouslyInLove,
    duration: '3:56',
    is_published: true,
  }, {
    title: 'Naughty girl ',
    album: dangerouslyInLove,
    duration: '4:02',
    is_published: true,
  }, {
    title: 'Hip hop star',
    album: dangerouslyInLove,
    duration: '3:35',
    is_published: true,
  }, {
    title: 'Yes',
    album: dangerouslyInLove,
    duration: '4:19',
    is_published: true,
  });

  await User.create({
    email: 'pops@mail.ru',
    password: '123',
    avatar: 'user.jpg',
    displayName: 'Jack Doe',
    token: nanoid(),
    role: 'User'
  }, {
    email: 'admin@mail.ru',
    password: '456',
    avatar: 'admin.jpg',
    displayName: 'Admin',
    token: nanoid(),
    role: 'Admin'
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));
const express = require('express');
const Track = require("../models/Track");
const mongoose = require("mongoose");
const Album = require("../models/Album");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    if (req.query.album) {
      const tracksInAlbum = await Track.find({album: req.query.album});
      return res.send(tracksInAlbum);
    }
    if (req.query.artist) {
      const albums = await Album.find({artist: req.query.artist});
      const idArray = albums.map(p => p._id);
      const tracks = await Track.find({album: idArray});
      return res.send(tracks);
    }
    const tracks = await Track.find();
    return res.send(tracks);
  } catch (e) {
    next(e);
  }
});

router.get('/byAlbum/:id', async (req, res, next) => {
  try {
    const albumData = await Album.findById(req.params.id).populate('artist');
    const tracksArray = await Track.find({album: req.params.id});
    const artistData = albumData.artist;
    const fullInfo = {
      artist: artistData,
      album: albumData,
      tracks: tracksArray
    };
    return res.send(fullInfo);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, async (req, res, next) => {
  try {
    if (!req.body.title || !req.body.album || !req.body.duration) {
      return res.status(400).send({message: 'The title, album and duration are required!'});
    }
    const trackData = {
      title: req.body.title,
      album: req.body.album,
      duration: req.body.duration,
      url: req.body.url
    };
    if(req.user.role === 'Admin') {
      trackData.is_published = true;
    }
    const newTrack = new Track(trackData);
    await newTrack.save();
    return res.send(newTrack);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/:id/publish', auth, permit('Admin'), async (req, res, next) => {
  try {
    const track = await Track.findOneAndUpdate({_id: req.body.trackId}, {is_published: true},
      {new: true});
    return res.send(track);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, permit('Admin'), async (req, res, next) => {
  try {
    await Track.deleteOne({_id: req.params.id});
    const tracks = await Track.find();
    return res.send(tracks);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
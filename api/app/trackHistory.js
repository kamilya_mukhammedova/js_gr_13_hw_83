const express = require('express');
const mongoose = require("mongoose");
const auth = require("../middleware/auth");
const TrackHistory = require("../models/TrackHistory");
const Album = require("../models/Album");
const router = express.Router();

router.get('/', auth, async (req, res, next) => {
  try {
    const fullInfoArray = [];
    const tracksHistory = await TrackHistory.find({user: req.user._id}).populate('track', 'title album');
    for(let i = 0; i < tracksHistory.length; i++) {
      const album = await Album.findById(tracksHistory[i].track.album).populate('artist', 'name');
      const trackInfo = {
        _id: tracksHistory[i]._id,
        trackTitle: tracksHistory[i].track.title,
        artistName: album.artist.name,
        datetime: tracksHistory[i].datetime
      };
      fullInfoArray.push(trackInfo);
    }
    return res.send(fullInfoArray.reverse());
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, async (req, res, next) => {
  try {
    if (!req.body.track) {
      return res.status(400).send({message: 'Track is required!'});
    }
    const trackHistoryData = {
      user: req.user._id,
      track: req.body.track,
      datetime: new Date().toISOString()
    };
   const trackHistory = new TrackHistory(trackHistoryData);
   await trackHistory.save();
   return res.send(trackHistory);
  } catch (e) {
    if(e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

module.exports = router;
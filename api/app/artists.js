const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Track = require("../models/Track");
const Album = require("../models/Album");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const artists = await Artist.find();
    return res.send(artists);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.name || !req.body.information) {
      return res.status(400).send({message: 'The name and information of artist are required!'});
    }
    const dataOfArtist = {
      name: req.body.name,
      image: null,
      information: req.body.information,
    };
    if (req.file) {
      dataOfArtist.image = req.file.filename;
    }
    if (req.user.role === 'Admin') {
      dataOfArtist.is_published = true;
    }
    const newArtist = new Artist(dataOfArtist);
    await newArtist.save();
    return res.send(newArtist);
  } catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('Admin'), async (req, res, next) => {
  try {
   const artist = await Artist.findOneAndUpdate({_id: req.body.artistId}, {is_published: true},
     {new: true});
   return res.send(artist);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, permit('Admin'), async (req, res, next) => {
  try {
    const artistAlbums = await Album.find({artist: req.params.id});
    for (let i = 0; i < artistAlbums.length; i++) {
      const tracksInAlbum = await Track.find({album: artistAlbums[i]._id});
      for (let i = 0; i < tracksInAlbum.length; i++) {
        await Track.deleteOne({_id: tracksInAlbum[i]._id});
      }
      await Album.deleteOne({_id: artistAlbums[i]._id});
    }

    await Artist.deleteOne({_id: req.params.id});
    const artists = await Artist.find();
    return res.send(artists);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const router = express.Router();
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Track = require("../models/Track");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    if (req.query.artist) {
      const albumsOfArtist = await Album.find({artist: req.query.artist});
      return res.send(albumsOfArtist);
    } else {
      const albums = await Album.find();
      return res.send(albums);
    }
  } catch (e) {
    next(e);
  }
});

router.get('/withArtist/:id', async (req, res, next) => {
  try {
    const allAlbums = await Album.find({artist: req.params.id});
    const artist = await Artist.findById(req.params.id);
    const albums = [{albums: allAlbums}]
    const fullInfo = [{artist: artist}, ...albums];
    return res.send(fullInfo);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const album = await Album.findById(req.params.id).populate('artist', 'name image information');
    if (!album) {
      return res.status(404).send({message: 'Album is not found'});
    }
    return res.send(album);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.body.artist || !req.body.yearOfIssue) {
      return res.status(400).send({message: 'The title, artist and year of issue are required'});
    }
    const albumData = {
      title: req.body.title,
      artist: req.body.artist,
      yearOfIssue: parseFloat(req.body.yearOfIssue),
      image: null,
    };
    if (req.file) {
      albumData.image = req.file.filename;
    }
    if (req.user.role === 'Admin') {
      albumData.is_published = true;
    }
    const newAlbum = new Album(albumData);
    await newAlbum.save();
    return res.send(newAlbum);
  } catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('Admin'), async (req, res, next) => {
  try {
    const album = await Album.findOneAndUpdate({_id: req.body.albumId}, {is_published: true},
      {new: true});
    return res.send(album);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, permit('Admin'), async (req, res, next) => {
  try {
    const tracksInAlbum = await Track.find({album: req.params.id});
    for (let i = 0; i < tracksInAlbum.length; i++) {
      await Track.deleteOne({_id: tracksInAlbum[i]._id});
    }

    await Album.deleteOne({_id: req.params.id});

    const albums = await Album.find();
    return res.send(albums);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  image: String,
  information: {
    type: String,
    required: true,
  },
  is_published: {
    type: Boolean,
    default: false
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);
module.exports = Artist;
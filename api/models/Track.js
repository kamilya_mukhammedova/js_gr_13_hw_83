const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true,
  },
  duration: {
    type: String,
    required: true,
  },
  url: {
    type: String
  },
  is_published: {
    type: Boolean,
    default: false
  }
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;
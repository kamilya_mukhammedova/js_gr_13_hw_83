import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { Track, TrackDataForEdit } from '../../models/track.model';
import { editTrackRequest, fetchTracksRequest, removeTrackRequest } from '../../store/tracks.actions';
import { ApiTrackHistoryData, TrackHistoryData } from '../../models/trackHistory.model';
import { addTrackHistoryRequest } from '../../store/tracksHistory.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalWindowComponent } from '../../ui/modal-window/modal-window.component';

export interface DialogData {
  trackUrl: string;
}

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.sass']
})
export class TracksComponent implements OnInit {
  tracks: Observable<Track[]>;
  tracksHistory: Observable<ApiTrackHistoryData[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  addToTrackHistoryError: Observable<null | string>;
  removeError: Observable<null | string>;
  albumTitle = '';
  editLoading: Observable<boolean>;
  editError: Observable<null | string>;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.tracks = store.select(state => state.tracks.tracks);
    this.loading = store.select(state => state.tracks.fetchLoading);
    this.error = store.select(state => state.tracks.fetchError);
    this.tracksHistory = store.select(state => state.tracksHistory.trackHistory);
    this.addToTrackHistoryError = store.select(state => state.tracksHistory.addToHistoryError);
    this.removeError = store.select(state => state.tracks.removeError);
    this.editLoading = store.select(state => state.tracks.editLoading);
    this.editError = store.select(state => state.tracks.editError);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const albumId = params['id'];
      this.albumTitle = params['albumTitle'];
      this.store.dispatch(fetchTracksRequest({albumId}));
    });
  }

  addToTrackHistory(trackId: string, url?: string) {
    if(url) {
      const dialogRef = this.dialog.open(ModalWindowComponent, {
        width: '500px',
        data: {trackUrl: url},
      });
    }
    const trackData: TrackHistoryData = {track: trackId};
    this.store.dispatch(addTrackHistoryRequest({trackHistoryData: trackData}));
  }


  onRemoveTrack(trackId: string) {
    this.store.dispatch(removeTrackRequest({trackId}));
  }

  onEdit(trackId: string) {
    const data: TrackDataForEdit = {
      trackId
    };
    this.store.dispatch(editTrackRequest({trackData: data}));
  }
}

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { TrackInfo } from '../../models/trackHistory.model';
import { fetchUserTracksRequest } from '../../store/tracksHistory.actions';

@Component({
  selector: 'app-tracks-history',
  templateUrl: './tracks-history.component.html',
  styleUrls: ['./tracks-history.component.sass']
})
export class TracksHistoryComponent implements OnInit {
  userTracks: Observable<TrackInfo[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(
    private store: Store<AppState>,
  ) {
    this.userTracks = store.select(state => state.tracksHistory.userTracks);
    this.loading = store.select(state => state.tracksHistory.fetchTracksLoading);
    this.error = store.select(state => state.tracksHistory.fetchTracksError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUserTracksRequest());
  }
}

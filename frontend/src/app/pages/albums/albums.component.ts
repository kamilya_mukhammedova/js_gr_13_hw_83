import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Album, AlbumDataForEdit } from '../../models/album.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute, Params } from '@angular/router';
import { editAlbumRequest, fetchAlbumsRequest, removeAlbumRequest } from '../../store/albums.actions';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.sass']
})
export class AlbumsComponent implements OnInit {
  albums: Observable<Album[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  removeLoading: Observable<boolean>;
  removeError: Observable<null | string>;
  artistName = '';
  editLoading: Observable<boolean>;
  editError: Observable<null | string>;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
    ) {
    this.albums = store.select(state => state.albums.albums);
    this.loading = store.select(state => state.albums.fetchLoading);
    this.error = store.select(state => state.albums.fetchError);
    this.removeLoading = store.select(state => state.albums.removeLoading);
    this.removeError = store.select(state => state.albums.removeError);
    this.editLoading = store.select(state => state.albums.editLoading);
    this.editError = store.select(state => state.albums.editError);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const artistId = params['id'];
      this.artistName = params['name'];
      this.store.dispatch(fetchAlbumsRequest({artistId}));
    });
  }

  onRemove(albumId: string) {
    this.store.dispatch(removeAlbumRequest({albumId}));
  }

  onEdit(albumId: string) {
    const data: AlbumDataForEdit = {
      albumId
    };
    this.store.dispatch(editAlbumRequest({albumData: data}));
  }
}

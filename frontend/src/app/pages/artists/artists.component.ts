import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Artist, ArtistDataForEdit } from '../../models/artist.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { editArtistRequest, fetchArtistsRequest, removeArtistRequest } from '../../store/artists.actions';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.sass']
})
export class ArtistsComponent implements OnInit {
  artists: Observable<Artist[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  removeLoading: Observable<boolean>;
  removeError: Observable<null | string>;
  editLoading: Observable<boolean>;
  editError: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.artists = store.select(state => state.artists.artists);
    this.loading = store.select(state => state.artists.fetchLoading);
    this.error = store.select(state => state.artists.fetchError);
    this.removeLoading = store.select(state => state.artists.removeLoading);
    this.removeError = store.select(state => state.artists.removeError);
    this.editLoading = store.select(state => state.artists.editLoading);
    this.editError = store.select(state => state.artists.editError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  onRemove(artistId: string) {
    this.store.dispatch(removeArtistRequest({artistId}));
  }

  onEdit(artistId: string) {
    const data: ArtistDataForEdit = {
      artistId
    };
    this.store.dispatch(editArtistRequest({artistData: data}));
  }
}

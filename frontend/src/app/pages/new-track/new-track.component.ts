import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { Artist } from '../../models/artist.model';
import { NgForm } from '@angular/forms';
import { fetchArtistsRequest } from '../../store/artists.actions';
import { Album } from '../../models/album.model';
import { fetchAlbumsRequest } from '../../store/albums.actions';
import { TracksService } from '../../services/tracks.service';
import { TrackData } from '../../models/track.model';
import { createTrackRequest } from '../../store/tracks.actions';

@Component({
  selector: 'app-new-track',
  templateUrl: './new-track.component.html',
  styleUrls: ['./new-track.component.sass']
})
export class NewTrackComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  artists: Observable<Artist[]>;
  albums: Observable<Album[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>, private track: TracksService) {
    this.artists = store.select(state => state.artists.artists);
    this.albums = store.select(state => state.albums.albums);
    this.loading = store.select(state => state.tracks.createLoading);
    this.error = store.select(state => state.tracks.createError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  onArtist() {
    const artistId = this.form.value.artist;
    this.store.dispatch(fetchAlbumsRequest({artistId}));
  }

  onSubmit() {
    const trackData: TrackData = {
      title: this.form.value.title,
      album: this.form.value.album,
      duration: this.form.value.duration,
      url: this.form.value.url,
    };
    this.store.dispatch(createTrackRequest({trackData}));
  }
}

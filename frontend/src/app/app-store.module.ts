import { NgModule } from '@angular/core';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { artistsReducer } from './store/artists.reducer';
import { albumsReducer } from './store/albums.reducer';
import { usersReducer } from './store/users.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ArtistsEffects } from './store/artists.effects';
import { AlbumsEffects } from './store/albums.effects';
import { UsersEffects } from './store/users.effects';
import { localStorageSync } from 'ngrx-store-localstorage';
import { TracksEffects } from './store/tracks.effects';
import { tracksReducer } from './store/tracks.reducer';
import { TracksHistoryEffects } from './store/tracksHistory.effects';
import { tracksHistoryReducer } from './store/tracksHistory.reducer';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
};

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  artists: artistsReducer,
  albums: albumsReducer,
  users: usersReducer,
  tracks: tracksReducer,
  tracksHistory: tracksHistoryReducer,
};

const effects = [ArtistsEffects, AlbumsEffects, UsersEffects, TracksEffects, TracksHistoryEffects];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule]
})
export class AppStoreModule{}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ArtistsComponent } from './pages/artists/artists.component';
import { AlbumsComponent } from './pages/albums/albums.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { ImagePipe } from './pipes/image.pipe';
import { RegisterComponent } from './pages/register/register.component';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ValidateIdenticalDirective } from './validate-indentical.directive';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { LoginComponent } from './pages/login/login.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { AppStoreModule } from './app-store.module';
import { TracksComponent } from './pages/tracks/tracks.component';
import { TracksHistoryComponent } from './pages/tracks-history/tracks-history.component';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalWindowComponent } from './ui/modal-window/modal-window.component';
import { SafePipe } from './pipes/safe.pipe';
import { NewArtistComponent } from './pages/new-artist/new-artist.component';
import { NewAlbumComponent } from './pages/new-album/new-album.component';
import { NewTrackComponent } from './pages/new-track/new-track.component';
import { MatSelectModule } from '@angular/material/select';
import { AuthInterceptor } from './auth.interceptor';
import { HasRolesDirective } from './directives/has-roles.directive';
import { IsPublishedDirective } from './directives/is-published.directive';
import { ButtonsDirective } from './directives/buttons.directive';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbAppID, {
        scope: 'email,public_profile'
      })
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent,
    ArtistsComponent,
    AlbumsComponent,
    ImagePipe,
    RegisterComponent,
    ValidateIdenticalDirective,
    FileInputComponent,
    LoginComponent,
    CenteredCardComponent,
    TracksComponent,
    TracksHistoryComponent,
    ModalWindowComponent,
    SafePipe,
    NewArtistComponent,
    NewAlbumComponent,
    NewTrackComponent,
    HasRolesDirective,
    IsPublishedDirective,
    ButtonsDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatToolbarModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatMenuModule,
    AppRoutingModule,
    AppStoreModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatSelectModule,
    SocialLoginModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

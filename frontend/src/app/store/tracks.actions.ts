import { createAction, props } from '@ngrx/store';
import { Track, TrackData, TrackDataForEdit } from '../models/track.model';

export const fetchTracksRequest = createAction(
  '[Tracks] Fetch Request',
  props<{albumId: string}>()
);
export const fetchTracksSuccess = createAction(
  '[Tracks] Fetch Success',
  props<{tracks: Track[]}>()
);
export const fetchTracksFailure = createAction(
  '[Tracks] Fetch Failure',
  props<{error: string}>()
);
export const createTrackRequest = createAction(
  '[Track] Create Request',
  props<{trackData: TrackData}>()
);
export const createTrackSuccess = createAction(
  '[Track] Create Success'
);
export const createTrackFailure = createAction(
  '[Track] Create Failure',
  props<{error: string}>()
);
export const removeTrackRequest = createAction(
  '[Track] Delete Request',
  props<{trackId: string}>()
);
export const removeTrackSuccess = createAction(
  '[Track] Delete Success'
);
export const removeTrackFailure = createAction(
  '[Track] Delete Failure',
  props<{error: string}>()
);
export const editTrackRequest = createAction(
  '[Track] Edit Request',
  props<{trackData: TrackDataForEdit}>()
);
export const editTrackSuccess = createAction(
  '[Track] Edit Success'
);
export const editTrackFailure = createAction(
  '[Track] Edit Failure',
  props<{error: string}>()
);

import { TracksState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTrackFailure,
  createTrackRequest,
  createTrackSuccess,
  editTrackFailure,
  editTrackRequest,
  editTrackSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess,
  removeTrackFailure,
  removeTrackRequest,
  removeTrackSuccess
} from './tracks.actions';

const initialState: TracksState = {
  tracks: [],
  albumId: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  removeLoading: false,
  removeError: null,
  editLoading: false,
  editError: null,
};

export const tracksReducer = createReducer(
  initialState,
  on(fetchTracksRequest, (state, {albumId}) => ({
    ...state,
    fetchLoading: true,
    albumId
  })),
  on(fetchTracksSuccess, (state, {tracks}) => ({
    ...state,
    fetchLoading: false,
    tracks
  })),
  on(fetchTracksFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createTrackRequest, state => ({...state, createLoading: true})),
  on(createTrackSuccess, state => ({...state, createLoading: false})),
  on(createTrackFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(removeTrackRequest, state => ({
    ...state,
    removeLoading: true,
  })),
  on(removeTrackSuccess, state => ({
    ...state,
    removeLoading: false,
  })),
  on(removeTrackFailure, (state, {error}) => ({
    ...state,
    removeLoading: false,
    removeError: error
  })),
  on(editTrackRequest, state => ({
    ...state,
    editLoading: true,
  })),
  on(editTrackSuccess, state => ({
    ...state,
    editLoading: false,
  })),
  on(editTrackFailure, (state, {error}) => ({
    ...state,
    editLoading: false,
    editError: error
  })),
);

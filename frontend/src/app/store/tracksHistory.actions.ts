import { createAction, props } from '@ngrx/store';
import { ApiTrackHistoryData, TrackHistoryData, TrackInfo } from '../models/trackHistory.model';

export const addTrackHistoryRequest = createAction(
  '[Track] Add Track History Request',
  props<{ trackHistoryData: TrackHistoryData }>()
);
export const addTrackHistorySuccess = createAction(
  '[Track] Add Track History Success',
  props<{ trackHistory: ApiTrackHistoryData[] }>()
);
export const addTrackHistoryFailure = createAction(
  '[Track] Add Track History Failure',
  props<{ error: string }>()
);
export const fetchUserTracksRequest = createAction('[Tracks] Fetch User Tracks Request');
export const fetchUserTracksSuccess = createAction(
  '[Tracks] Fetch User Tracks Success',
  props<{ userTracks: TrackInfo[] }>()
);
export const fetchUserTracksFailure = createAction(
  '[Tracks] Fetch User Tracks Failure',
  props<{ error: string }>()
);



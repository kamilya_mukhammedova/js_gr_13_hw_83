import { createAction, props } from '@ngrx/store';
import { Artist, ArtistData, ArtistDataForEdit } from '../models/artist.model';

export const fetchArtistsRequest = createAction('[Artists] Fetch Request');
export const fetchArtistsSuccess = createAction(
  '[Artists] Fetch Success',
  props<{artists: Artist[]}>()
);
export const fetchArtistsFailure = createAction(
  '[Artists] Fetch Failure',
  props<{error: string}>()
);
export const createArtistRequest = createAction(
  '[Artist] Create Request',
  props<{artistData: ArtistData}>()
);
export const createArtistSuccess = createAction(
  '[Artist] Create Success'
);
export const createArtistFailure = createAction(
  '[Artist] Create Failure',
  props<{error: string}>()
);
export const removeArtistRequest = createAction(
  '[Artist] Delete Request',
  props<{artistId: string}>()
);
export const removeArtistSuccess = createAction(
  '[Artist] Delete Success'
);
export const removeArtistFailure = createAction(
  '[Artist] Delete Failure',
  props<{error: string}>()
);
export const editArtistRequest = createAction(
  '[Artist] Edit Request',
  props<{artistData: ArtistDataForEdit}>()
);
export const editArtistSuccess = createAction(
  '[Artist] Edit Success'
);
export const editArtistFailure = createAction(
  '[Artist] Edit Failure',
  props<{error: string}>()
);



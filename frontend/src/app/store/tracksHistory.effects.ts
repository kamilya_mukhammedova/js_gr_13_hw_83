import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TrackHistoryService } from '../services/track-history.service';
import {
  addTrackHistoryFailure,
  addTrackHistoryRequest,
  addTrackHistorySuccess, fetchUserTracksFailure,
  fetchUserTracksRequest, fetchUserTracksSuccess
} from './tracksHistory.actions';
import { Store } from '@ngrx/store';
import { AppState } from './types';
import { catchError, map, mergeMap, NEVER, of, tap, withLatestFrom } from 'rxjs';
import { HelpersService } from '../services/helpers.service';
import { Router } from '@angular/router';

@Injectable()
export class TracksHistoryEffects {
  constructor(
    private actions: Actions,
    private tracksHistoryService: TrackHistoryService,
    private store: Store<AppState>,
    private helpers: HelpersService,
    private router: Router
    ) {}

  addTracksHistory = createEffect(() => this.actions.pipe(
    ofType(addTrackHistoryRequest),
    withLatestFrom(this.store.select(state => state.users.user)),
    mergeMap(([{trackHistoryData}, user]) => {
      if(user) {
        return this.tracksHistoryService.addTrackHistory(trackHistoryData, user.token).pipe(
          map(trackHistory => addTrackHistorySuccess({trackHistory})),
          tap(() => this.helpers.openSnackbar('Added in track history')),
          catchError(() => of(addTrackHistoryFailure({
            error: 'Something went wrong! Can\'t add track in tracks history!'
          })))
        );
      }
      this.helpers.openSnackbar('Need to register or login');
      return NEVER;
    }),
  ));

  getUserTracks = createEffect(() => this.actions.pipe(
    ofType(fetchUserTracksRequest),
    withLatestFrom(this.store.select(state => state.users.user)),
    mergeMap(([_, user]) => {
      if(user) {
        return this.tracksHistoryService.getUserTrackHistory(user.token).pipe(
          map(userTracks => fetchUserTracksSuccess({userTracks})),
          catchError(() => of(fetchUserTracksFailure({
            error: 'Something went wrong! Can\'t upload tracks in user track history!'
          })))
        );
      }
      void this.router.navigate(['/login']);
      return NEVER;
    }),
  ));
}

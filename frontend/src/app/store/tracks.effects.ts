import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TracksService } from '../services/tracks.service';
import {
  createTrackFailure,
  createTrackRequest,
  createTrackSuccess,
  editTrackFailure,
  editTrackRequest,
  editTrackSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess,
  removeTrackFailure,
  removeTrackRequest
} from './tracks.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helpers.service';

@Injectable()
export class TracksEffects {
  constructor(
    private actions: Actions,
    private tracksService: TracksService,
    private router: Router,
    private helpers: HelpersService
   ) {}

  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(({albumId}) => this.tracksService.getTracksInAlbum(albumId).pipe(
      map(tracks => fetchTracksSuccess({tracks})),
      catchError(() => of(fetchTracksFailure({
        error: "Something went wrong! Can't upload the list of tracks!"
      })))
    ))
  ));

  createTrack = createEffect(() => this.actions.pipe(
    ofType(createTrackRequest),
    mergeMap(({trackData}) => this.tracksService.createTrack(trackData).pipe(
      map(() => createTrackSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.helpers.openSnackbar('Added new track');
      }),
      catchError(() => of(createTrackFailure({error: 'Wrong data of track'})))
    ))
  ));

  removeTrack = createEffect(() => this.actions.pipe(
    ofType(removeTrackRequest),
    mergeMap(({trackId}) => this.tracksService.removeTrack(trackId).pipe(
      map(() => createTrackSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.helpers.openSnackbar('Removed track');
      }),
      catchError(() => of(removeTrackFailure({
        error: 'Something went wrong! Can\'t remove track!'
      })))
    ))
  ));

  editTrack = createEffect(() => this.actions.pipe(
    ofType(editTrackRequest),
    mergeMap(({trackData}) => this.tracksService.editTrack(trackData).pipe(
      map(() => editTrackSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.helpers.openSnackbar('Edit track');
      }),
      catchError(() => of(editTrackFailure({
        error: 'Something went wrong! Can\'t edit track!'
      })))
    ))
  ));
}

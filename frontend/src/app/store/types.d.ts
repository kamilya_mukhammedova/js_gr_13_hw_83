import { Artist } from '../models/artist.model';
import { Album } from '../models/album.model';
import { LoginError, RegisterError, User } from '../models/user.model';
import { Track } from '../models/track.model';
import { ApiTrackHistoryData, TrackInfo } from '../models/trackHistory.model';

export type ArtistsState = {
  artists: Artist[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  removeLoading: boolean,
  removeError: null | string,
  editLoading: boolean,
  editError: null | string,
};

export type AlbumState = {
  albums: Album[],
  artistId: null | string,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  removeLoading: boolean,
  removeError: null | string,
  editLoading: boolean,
  editError: null | string,
};

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
  fbLoading: boolean,
  fbError: null | string,
};

export type TracksState = {
  tracks: Track[],
  albumId: null | string,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  removeLoading: boolean,
  removeError: null | string,
  editLoading: boolean,
  editError: null | string,
};

export type TracksHistoryState = {
  trackHistory: ApiTrackHistoryData[],
  addToHistoryLoading: boolean,
  addToHistoryError: null | string,
  userTracks: TrackInfo[],
  fetchTracksLoading: boolean,
  fetchTracksError: null | string,
};

export type AppState = {
  albums: AlbumState,
  artists: ArtistsState,
  users: UsersState,
  tracks: TracksState,
  tracksHistory: TracksHistoryState,
};

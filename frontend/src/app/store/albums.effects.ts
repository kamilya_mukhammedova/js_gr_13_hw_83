import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { LastFmService } from '../services/last-fm.service';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  createAlbumFailure,
  createAlbumRequest,
  createAlbumSuccess,
  editAlbumFailure,
  editAlbumRequest,
  editAlbumSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  removeAlbumFailure,
  removeAlbumRequest,
  removeAlbumSuccess
} from './albums.actions';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helpers.service';

@Injectable()
export class AlbumsEffects {
  constructor(
    private actions: Actions,
    private lastFmService: LastFmService,
    private router: Router,
    private helpers: HelpersService,
  ) {
  }

  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(({artistId}) => this.lastFmService.getAlbums(artistId).pipe(
      map(albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({
        error: 'Something went wrong! Can\'t upload the list of albums!'
      })))
    ))
  ));

  createAlbum = createEffect(() => this.actions.pipe(
    ofType(createAlbumRequest),
    mergeMap(({albumData}) => this.lastFmService.createAlbum(albumData).pipe(
      map(() => createAlbumSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.helpers.openSnackbar('Created new album');
      }),
      catchError(() => of(createAlbumFailure({error: 'Wrong data of album'})))
    ))
  ));

  removeAlbum = createEffect(() => this.actions.pipe(
    ofType(removeAlbumRequest),
    mergeMap(({albumId}) => this.lastFmService.removeAlbum(albumId).pipe(
      map(() => removeAlbumSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.helpers.openSnackbar('Removed album');
      }),
      catchError(() => of(removeAlbumFailure({
        error: 'Something went wrong! Can\'t remove album!'
      })))
    ))
  ));

  editAlbum = createEffect(() => this.actions.pipe(
    ofType(editAlbumRequest),
    mergeMap(({albumData}) => this.lastFmService.editAlbum(albumData).pipe(
      map(() => editAlbumSuccess()),
      tap(() => {
        void this.router.navigate(['/']);
        this.helpers.openSnackbar('Edit album');
      }),
      catchError(() => of(editAlbumFailure({
        error: 'Something went wrong! Can\'t edit album!'
      })))
    ))
  ));
}

import { TracksHistoryState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  addTrackHistoryFailure,
  addTrackHistoryRequest,
  addTrackHistorySuccess, fetchUserTracksFailure,
  fetchUserTracksRequest, fetchUserTracksSuccess
} from './tracksHistory.actions';

const initialState: TracksHistoryState = {
  trackHistory: [],
  addToHistoryLoading: false,
  addToHistoryError: null,
  userTracks: [],
  fetchTracksLoading: false,
  fetchTracksError: null,
};

export const tracksHistoryReducer = createReducer(
  initialState,
  on(addTrackHistoryRequest, state => ({
    ...state,
    addToHistoryLoading: true,
    addToHistoryError: null
  })),
  on(addTrackHistorySuccess, (state, {trackHistory}) => ({
    ...state,
    registerLoading: false,
    trackHistory
  })),
  on(addTrackHistoryFailure, (state, {error}) => ({
    ...state,
    addToHistoryLoading: false,
    addToHistoryError: error
  })),
  on(fetchUserTracksRequest, state => ({
    ...state,
    fetchTracksLoading: true
  })),
  on(fetchUserTracksSuccess, (state, {userTracks}) => ({
    ...state,
    fetchTracksLoading: false,
    userTracks
  })),
  on(fetchUserTracksFailure, (state, {error}) => ({
    ...state,
    fetchTracksLoading: false,
    fetchTracksError: error
  })),
);

import { createAction, props } from '@ngrx/store';
import { Album, AlbumData, AlbumDataForEdit } from '../models/album.model';

export const fetchAlbumsRequest = createAction(
  '[Albums] Fetch Request',
  props<{artistId: string}>()
);
export const fetchAlbumsSuccess = createAction(
  '[Albums] Fetch Success',
  props<{albums: Album[]}>()
);
export const fetchAlbumsFailure = createAction(
  '[Albums] Fetch Failure',
  props<{error: string}>()
);
export const createAlbumRequest = createAction(
  '[Album] Create Request',
  props<{albumData: AlbumData}>()
);
export const createAlbumSuccess = createAction(
  '[Album] Create Success'
);
export const createAlbumFailure = createAction(
  '[Album] Create Failure',
  props<{error: string}>()
);
export const removeAlbumRequest = createAction(
  '[Album] Delete Request',
  props<{albumId: string}>()
);
export const removeAlbumSuccess = createAction(
  '[Album] Delete Success'
);
export const removeAlbumFailure = createAction(
  '[Album] Delete Failure',
  props<{error: string}>()
);
export const editAlbumRequest = createAction(
  '[Album] Edit Request',
  props<{albumData: AlbumDataForEdit}>()
);
export const editAlbumSuccess = createAction(
  '[Album] Edit Success'
);
export const editAlbumFailure = createAction(
  '[Album] Edit Failure',
  props<{error: string}>()
);

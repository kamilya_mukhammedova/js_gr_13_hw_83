import { AlbumState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createAlbumFailure,
  createAlbumRequest,
  createAlbumSuccess,
  editAlbumFailure,
  editAlbumRequest,
  editAlbumSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  removeAlbumFailure,
  removeAlbumRequest,
  removeAlbumSuccess
} from './albums.actions';

const initialState: AlbumState = {
  albums: [],
  artistId: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  removeLoading: false,
  removeError: null,
  editLoading: false,
  editError: null,
};

export const albumsReducer = createReducer(
  initialState,
  on(fetchAlbumsRequest, (state, {artistId}) => ({
    ...state,
    fetchLoading: true,
    artistId
  })),
  on(fetchAlbumsSuccess, (state, {albums}) => ({
    ...state,
    fetchLoading: false,
    albums
  })),
  on(fetchAlbumsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createAlbumRequest, state => ({...state, createLoading: true})),
  on(createAlbumSuccess, state => ({...state, createLoading: false})),
  on(createAlbumFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(removeAlbumRequest, state => ({
    ...state,
    removeLoading: true,
  })),
  on(removeAlbumSuccess, state => ({
    ...state,
    removeLoading: false,
  })),
  on(removeAlbumFailure, (state, {error}) => ({
    ...state,
    removeLoading: false,
    removeError: error
  })),
  on(editAlbumRequest, state => ({
    ...state,
    editLoading: true,
  })),
  on(editAlbumSuccess, state => ({
    ...state,
    editLoading: false,
  })),
  on(editAlbumFailure, (state, {error}) => ({
    ...state,
    editLoading: false,
    editError: error
  })),
);

import { ArtistsState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess, editArtistFailure, editArtistRequest, editArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess,
  removeArtistFailure,
  removeArtistRequest,
  removeArtistSuccess
} from './artists.actions';

const initialState: ArtistsState = {
  artists: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  removeLoading: false,
  removeError: null,
  editLoading: false,
  editError: null,
};

export const artistsReducer = createReducer(
  initialState,
  on(fetchArtistsRequest, state => ({...state, fetchLoading: true})),
  on(fetchArtistsSuccess, (state, {artists}) => ({
    ...state,
    fetchLoading: false,
    artists
  })),
  on(fetchArtistsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createArtistRequest, state => ({...state, createLoading: true})),
  on(createArtistSuccess, state => ({...state, createLoading: false})),
  on(createArtistFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(removeArtistRequest, state => ({
    ...state,
    removeLoading: true,
  })),
  on(removeArtistSuccess, state => ({
    ...state,
    removeLoading: false,
  })),
  on(removeArtistFailure, (state, {error}) => ({
    ...state,
    removeLoading: false,
    removeError: error
  })),
  on(editArtistRequest, state => ({
    ...state,
    editLoading: true,
  })),
  on(editArtistSuccess, state => ({
    ...state,
    editLoading: false,
  })),
  on(editArtistFailure, (state, {error}) => ({
    ...state,
    editLoading: false,
    editError: error
  })),
);

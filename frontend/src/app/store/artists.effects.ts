import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { LastFmService } from '../services/last-fm.service';
import {
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess, editArtistFailure, editArtistRequest, editArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess,
  removeArtistFailure,
  removeArtistRequest,
  removeArtistSuccess
} from './artists.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helpers.service';
import { Store } from '@ngrx/store';
import { AppState } from './types';

@Injectable()
export class ArtistsEffects {
  constructor(
    private actions: Actions,
    private lastFmService: LastFmService,
    private router: Router,
    private helpers: HelpersService,
    private store: Store<AppState>
  ) {}

  fetchArtists = createEffect(() => this.actions.pipe(
    ofType(fetchArtistsRequest),
    mergeMap(() => this.lastFmService.getArtists().pipe(
      map(artists => fetchArtistsSuccess({artists})),
      catchError(() => of(fetchArtistsFailure({
        error: "Something went wrong! Can't upload the list of artists!"
      })))
    ))
  ));

  createArtist = createEffect(() => this.actions.pipe(
    ofType(createArtistRequest),
    mergeMap(({artistData}) => this.lastFmService.createArtist(artistData).pipe(
      map(() => createArtistSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createArtistFailure({error: 'Wrong data of artist'})))
    ))
  ));

  removeArtist = createEffect(() => this.actions.pipe(
    ofType(removeArtistRequest),
    mergeMap(({artistId}) => this.lastFmService.removeArtist(artistId).pipe(
      map(() => removeArtistSuccess()),
      tap(() => {
        this.store.dispatch(fetchArtistsRequest());
        this.helpers.openSnackbar('Removed artist');
      }),
      catchError(() => of(removeArtistFailure({
        error: 'Something went wrong! Can\'t remove artist!'
      })))
    ))
  ));

  editArtist = createEffect(() => this.actions.pipe(
    ofType(editArtistRequest),
    mergeMap(({artistData}) => this.lastFmService.editArtist(artistData).pipe(
      map(() => editArtistSuccess()),
      tap(() => {
        this.store.dispatch(fetchArtistsRequest());
        this.helpers.openSnackbar('Edit artist');
      }),
      catchError(() => of(editArtistFailure({
        error: 'Something went wrong! Can\'t edit artist!'
      })))
    ))
  ));
}

export class TrackInfo {
  constructor(
    public _id: string,
    public trackTitle: string,
    public artistName: string,
    public datetime: string
  ) {}
}

export interface ApiTrackHistoryData {
  _id: string,
  user: string,
  track: string,
  datetime: string,
}

export interface TrackHistoryData {
  track: string,
}

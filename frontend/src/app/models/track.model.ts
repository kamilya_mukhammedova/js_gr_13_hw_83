export class Track {
  constructor(
    public id: string,
    public title: string,
    public album: string,
    public duration: string,
    public is_published: boolean,
    public url?: string,
  ) {}
}

export interface ApiTrackData {
  _id: string,
  title: string,
  album: string,
  duration: string,
  is_published: boolean,
  url?: string,
}

export interface TrackData {
  title: string,
  album: string,
  duration: string,
  url: string,
}

export interface TrackDataForEdit {
  trackId: string
}



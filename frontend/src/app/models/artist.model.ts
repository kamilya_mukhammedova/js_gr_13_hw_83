export class Artist {
  constructor(
    public id: string,
    public name: string,
    public image: string,
    public information: string,
    public is_published: boolean,
  ) {}
}

export interface ApiArtistData {
  _id: string,
  name: string,
  image: string,
  information: string,
  is_published: boolean,
}

export interface ArtistData {
  [key: string]: any,
  name: string,
  image: File | null,
  information: string,
}

export interface ArtistDataForEdit {
  artistId: string
}

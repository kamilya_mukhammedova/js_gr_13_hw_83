import { Artist } from './artist.model';

export class Album {
  constructor(
    public id: string,
    public title: string,
    public artist: Artist,
    public yearOfIssue: number,
    public image: string,
    public is_published: boolean,
  ) {}
}

export interface ApiAlbumData {
  _id: string;
  title: string;
  artist: Artist,
  yearOfIssue: number,
  image: string,
  is_published: boolean,
}

export interface AlbumData {
  [key: string]: any,
  title: string,
  artist: string,
  yearOfIssue: number,
  image: File | null,
}

export interface AlbumDataForEdit {
  albumId: string
}

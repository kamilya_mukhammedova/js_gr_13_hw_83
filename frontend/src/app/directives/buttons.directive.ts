import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable, Subscription } from 'rxjs';
import { User } from '../models/user.model';

@Directive({
  selector: '[appButtons]'
})
export class ButtonsDirective implements OnInit, OnDestroy{
  @Input('appButtons') is_published!: boolean;
  userSubscription!: Subscription;

  user: Observable<null | User>;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSubscription = this.user.subscribe(user => {
      this.viewContainer.clear();

      if (user && user.role === 'Admin') {
        if(!this.is_published) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}

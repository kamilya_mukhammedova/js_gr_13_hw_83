import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiArtistData, Artist, ArtistData, ArtistDataForEdit } from '../models/artist.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';
import { Album, AlbumData, AlbumDataForEdit, ApiAlbumData } from '../models/album.model';

@Injectable({
  providedIn: 'root'
})
export class LastFmService {
  constructor(private http: HttpClient) {}

  getArtists() {
    return this.http.get<ApiArtistData[]>(environment.apiUrl + '/artists').pipe(
      map(response => {
        if (response.length === 0) {
          return [];
        }
        return response.map(artistData => {
          return new Artist(
            artistData._id,
            artistData.name,
            artistData.image,
            artistData.information,
            artistData.is_published
          );
        });
      })
    );
  }

  createArtist(artistData: ArtistData) {
    const formData = new FormData();

    Object.keys(artistData).forEach(key => {
      if(artistData[key] !== null) {
        formData.append(key, artistData[key]);
      }
    });
    return this.http.post<ApiArtistData>(environment.apiUrl + '/artists', formData);
  }

  removeArtist(artistId: string) {
    return this.http.delete<ApiArtistData[]>(environment.apiUrl + `/artists/${artistId}`);
  }

  editArtist(artistData: ArtistDataForEdit) {
    return this.http.post<ApiArtistData>(environment.apiUrl + `/artists/${artistData.artistId}/publish`, artistData);
  }

  getAlbums(artistId: string) {
    return this.http.get<ApiAlbumData[]>(environment.apiUrl + `/albums?artist=${artistId}`).pipe(
      map(response => {
        if (response.length === 0) {
          return [];
        }
        return response.map(albumData => {
          return new Album(
            albumData._id,
            albumData.title,
            albumData.artist,
            albumData.yearOfIssue,
            albumData.image,
            albumData.is_published,
          );
        });
      })
    );
  }

  createAlbum(albumData: AlbumData) {
    const formData = new FormData();

    Object.keys(albumData).forEach(key => {
      if(albumData[key] !== null) {
        formData.append(key, albumData[key]);
      }
    });
    return this.http.post<ApiAlbumData>(environment.apiUrl + '/albums', formData);
  }

  removeAlbum(albumId: string) {
    return this.http.delete<ApiAlbumData[]>(environment.apiUrl + `/albums/${albumId}`);
  }

  editAlbum(albumData: AlbumDataForEdit) {
    return this.http.post<ApiAlbumData>(environment.apiUrl + `/albums/${albumData.albumId}/publish`, albumData);
  }
}

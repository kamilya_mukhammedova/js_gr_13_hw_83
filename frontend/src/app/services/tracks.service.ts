import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiTrackData, Track, TrackData, TrackDataForEdit } from '../models/track.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TracksService {
  constructor(private http: HttpClient) { }

  getTracksInAlbum(albumId: string) {
    return this.http.get<ApiTrackData[]>(environment.apiUrl + `/tracks?album=${albumId}`).pipe(
      map(response => {
        if (response.length === 0) {
          return [];
        }
        return response.map(trackData => {
          return new Track(
            trackData._id,
            trackData.title,
            trackData.album,
            trackData.duration,
            trackData.is_published,
            trackData.url,
          );
        });
      })
    );
  }

  createTrack(trackData: TrackData) {
    return this.http.post<ApiTrackData>(environment.apiUrl + '/tracks', trackData);
  }

  removeTrack(trackId: string) {
    return this.http.delete<ApiTrackData[]>(environment.apiUrl + `/tracks/${trackId}`);
  }

  editTrack(trackData: TrackDataForEdit) {
    return this.http.post<ApiTrackData>(environment.apiUrl + `/tracks/${trackData.trackId}/publish`, trackData);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiTrackHistoryData, TrackHistoryData, TrackInfo } from '../models/trackHistory.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrackHistoryService {
  constructor(private http: HttpClient) { }

  addTrackHistory(trackHistoryData: TrackHistoryData, token: string) {
    return this.http.post<ApiTrackHistoryData[]>(environment.apiUrl + '/track_history', trackHistoryData, {
      headers: new HttpHeaders({'Authorization': token})
    });
  }

  getUserTrackHistory(token: string) {
    return this.http.get<TrackInfo[]>(environment.apiUrl + '/track_history', {
      headers: new HttpHeaders({'Authorization': token})
    }).pipe(
      map(response => {
        if (response.length === 0) {
          return [];
        }
        return response.map(trackData => {
          return new TrackInfo(
            trackData._id,
            trackData.trackTitle,
            trackData.artistName,
            trackData.datetime
          );
        });
      })
    );
  }
}
